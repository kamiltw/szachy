package pk.szachy.gra;

import java.awt.Color;
import java.awt.Point;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import pk.szachy.grafika.IGrafika;

/**
 *
 * @author Frogy07
 */
public class Wieza extends Figura {

    Wieza(IGrafika grafika, String sciezkaPlikuGraficznego_lvl1, String sciezkaPlikuGraficznego_lvl2, boolean kolor_bialy, int poz_x, int poz_y) {
        super(grafika, sciezkaPlikuGraficznego_lvl1, sciezkaPlikuGraficznego_lvl2, kolor_bialy, poz_x, poz_y);
    }

    //zaznacz kolorami pola posuniecia,ataku i ataku na odleglosc
    @Override
    public void najechanoNaMnie() {
        //sprawdz czy jest tura innego koloru
        //jesli tak wyjdz
        if (tura_koloru != this.kolor_pionka) {
            return;
        }

        odegraj_dziwek(DZWIEK_NAJECHANIA_NA_FIGURE);

        //----------------------------------------------------------------------
        //                  KOD LOGIKI
        //======================================================================
        //rozstawienie poczatkowe biale na gorze szachownicy, czarne na dole
        int wspKoloru;
        if (kolor_pionka == BIALE) {
            wspKoloru = 1;
        } else {
            wspKoloru = -1;
        }

        if (level == 0) {
            //ruch
            {
                for (int i = 1; i <= 4; i++) {
                    if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y + i * wspKoloru)) == FIGURA_BRAK) {
                        PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y + i * wspKoloru), POLE_POSUNIECIA);

                    } else {
                        break;
                    }

                }
                for (int i = 1; i <= 4; i++) {
                    if (FiguraNaPozycji(new Point(moja_pozycja.x + i, moja_pozycja.y)) == FIGURA_BRAK) {
                        PokolorujPole(new Point(moja_pozycja.x + i, moja_pozycja.y), POLE_POSUNIECIA);
                    } else {
                        break;
                    }
                }
                for (int i = 1; i <= 4; i++) {
                    if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y - i * wspKoloru)) == FIGURA_BRAK) {
                        PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y - i * wspKoloru), POLE_POSUNIECIA);
                    } else {
                        break;
                    }
                }
                for (int i = 1; i <= 4; i++) {
                    if (FiguraNaPozycji(new Point(moja_pozycja.x - i, moja_pozycja.y)) == FIGURA_BRAK) {
                        PokolorujPole(new Point(moja_pozycja.x - i, moja_pozycja.y), POLE_POSUNIECIA);
                    } else {
                        break;
                    }
                }
            }

            //bicie bezposrednie
            {
                for (int i = 1; i <= 4; i++) {
                    if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y + i * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                        PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y + i * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                        break;
                    }
                }
                for (int i = 1; i <= 4; i++) {
                    if (FiguraNaPozycji(new Point(moja_pozycja.x + i, moja_pozycja.y)) == FIGURA_PRZECIWNIKA) {
                        PokolorujPole(new Point(moja_pozycja.x + i, moja_pozycja.y), POLE_ATAKU_BEZPOSREDNIEGO);
                        break;
                    }
                }
                for (int i = 1; i <= 4; i++) {
                    if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y - i * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                        PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y - i * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                        break;
                    }
                }
                for (int i = 1; i <= 4; i++) {
                    if (FiguraNaPozycji(new Point(moja_pozycja.x - i, moja_pozycja.y)) == FIGURA_PRZECIWNIKA) {
                        PokolorujPole(new Point(moja_pozycja.x - i, moja_pozycja.y), POLE_ATAKU_BEZPOSREDNIEGO);
                        break;
                    }
                }
            }

            //bicie na odleglosc - brak
            /*{
             if (FiguraNaPozycji(new Point(moja_pozycja.x - 1, moja_pozycja.y + 2 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
             PokolorujPole(new Point(moja_pozycja.x - 1, moja_pozycja.y + 2 * wspKoloru), POLE_ATAKU_POSREDNIEGO);
             }

             if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y + 2 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
             PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y + 2 * wspKoloru), POLE_ATAKU_POSREDNIEGO);
             }

             if (FiguraNaPozycji(new Point(moja_pozycja.x + 1, moja_pozycja.y + 2 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
             PokolorujPole(new Point(moja_pozycja.x + 1, moja_pozycja.y + 2 * wspKoloru), POLE_ATAKU_POSREDNIEGO);
             }
             }*/
        }

        if (level == 1) {
            //ruch
            {
                for (int i = 1; i <= 4; i++) {
                    if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y + i * wspKoloru)) == FIGURA_BRAK) {
                        PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y + i * wspKoloru), POLE_POSUNIECIA);

                    } else {
                        break;
                    }

                }
                for (int i = 1; i <= 4; i++) {
                    if (FiguraNaPozycji(new Point(moja_pozycja.x + i, moja_pozycja.y)) == FIGURA_BRAK) {
                        PokolorujPole(new Point(moja_pozycja.x + i, moja_pozycja.y), POLE_POSUNIECIA);
                    } else {
                        break;
                    }
                }
                for (int i = 1; i <= 4; i++) {
                    if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y - i * wspKoloru)) == FIGURA_BRAK) {
                        PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y - i * wspKoloru), POLE_POSUNIECIA);
                    } else {
                        break;
                    }
                }
                for (int i = 1; i <= 4; i++) {
                    if (FiguraNaPozycji(new Point(moja_pozycja.x - i, moja_pozycja.y)) == FIGURA_BRAK) {
                        PokolorujPole(new Point(moja_pozycja.x - i, moja_pozycja.y), POLE_POSUNIECIA);
                    } else {
                        break;
                    }
                }
            }

            //bicie bezposrednie
            {
                for (int i = 1; i <= 4; i++) {
                    if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y + i * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                        PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y + i * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                        break;
                    }
                }
                for (int i = 1; i <= 4; i++) {
                    if (FiguraNaPozycji(new Point(moja_pozycja.x + i, moja_pozycja.y)) == FIGURA_PRZECIWNIKA) {
                        PokolorujPole(new Point(moja_pozycja.x + i, moja_pozycja.y), POLE_ATAKU_BEZPOSREDNIEGO);
                        break;
                    }
                }
                for (int i = 1; i <= 4; i++) {
                    if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y - i * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                        PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y - i * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                        break;
                    }
                }
                for (int i = 1; i <= 4; i++) {
                    if (FiguraNaPozycji(new Point(moja_pozycja.x - i, moja_pozycja.y)) == FIGURA_PRZECIWNIKA) {
                        PokolorujPole(new Point(moja_pozycja.x - i, moja_pozycja.y), POLE_ATAKU_BEZPOSREDNIEGO);
                        break;
                    }
                }

            }

            //bicie na odleglosc - brak
            for (int i = 5; i <= 6; i++) {
                if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y + i * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y + i * wspKoloru), POLE_ATAKU_POSREDNIEGO);
                    break;
                }
            }
            for (int i = 5; i <= 6; i++) {
                if (FiguraNaPozycji(new Point(moja_pozycja.x + i, moja_pozycja.y)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x + i, moja_pozycja.y), POLE_ATAKU_POSREDNIEGO);
                    break;
                }
            }
            for (int i = 5; i <= 6; i++) {
                if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y - i * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y - i * wspKoloru), POLE_ATAKU_POSREDNIEGO);
                    break;
                }
            }
            for (int i = 5; i <= 6; i++) {
                if (FiguraNaPozycji(new Point(moja_pozycja.x - i, moja_pozycja.y)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x - i, moja_pozycja.y), POLE_ATAKU_POSREDNIEGO);
                    break;
                }
            }
        }
    }

    @Override
    protected void odegraj_dziwek(int dzwiek) {
        //odgrywa dzwiek w zaleznosci od tego co zostalo zrobione
        if (level == 0) {
            switch (dzwiek) {
        case POLE_NIEZAZNACZONE://nidozwolona pozycja
                muzyka.graj("C:\\szachy\\dz_idz.wav", 1);
                break;
            case POLE_POSUNIECIA://przesunieto
                muzyka.graj("C:\\szachy\\dz_idz.wav", 1);
                break;
            case POLE_ATAKU_BEZPOSREDNIEGO://atak
                muzyka.graj("C:\\szachy\\dz_atak_blisko.wav", 1);
                break;
            case POLE_ATAKU_POSREDNIEGO://atak
                muzyka.graj("C:\\szachy\\dz_atak_daleko.wav", 1);
                break;
            case DZWIEK_NAJECHANIA_NA_FIGURE://najechanie myszka
                muzyka.graj("C:\\szachy\\dz_najechanie.wav", 1);
                break;
            }
        } else {
            switch (dzwiek) {

                case POLE_NIEZAZNACZONE://nidozwolona pozycja
                    muzyka.graj("C:\\szachy\\dz_czolg_idz.wav", 1);
                    break;
                case POLE_POSUNIECIA://przesunieto
                    muzyka.graj("C:\\szachy\\dz_czolg_idz.wav", 1);
                    break;
                case POLE_ATAKU_BEZPOSREDNIEGO://atak
                    muzyka.graj("C:\\szachy\\dz_czolg_atak.wav", 1);
                    break;
                case POLE_ATAKU_POSREDNIEGO://atak
                    muzyka.graj("C:\\szachy\\dz_czolg_atak.wav", 1);
                    break;
                case DZWIEK_NAJECHANIA_NA_FIGURE://najechanie myszka
                    muzyka.graj("C:\\szachy\\dz_najechanie.wav", 1);
                    break;
            }
        }
    }
}
