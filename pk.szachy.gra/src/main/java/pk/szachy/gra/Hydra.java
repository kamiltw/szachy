/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.szachy.gra;
import pk.szachy.grafika.IGrafika;

import java.awt.Point;
import java.net.URL;

/**
 *
 * @author Frogy07
 */
public class Hydra extends Figura {

    Hydra(IGrafika grafika, String sciezkaPlikuGraficznego_lvl1, String sciezkaPlikuGraficznego_lvl2, boolean kolor_bialy, int poz_x, int poz_y) {
        super(grafika, sciezkaPlikuGraficznego_lvl1, sciezkaPlikuGraficznego_lvl2, kolor_bialy, poz_x, poz_y);
    }

    //zaznacz kolorami pola posuniecia,ataku i ataku na odleglosc
    @Override
    public void najechanoNaMnie() {
        //sprawdz czy jest tura innego koloru
        //jesli tak wyjdz
        if (tura_koloru != this.kolor_pionka) {
            return;
        }

        odegraj_dziwek(DZWIEK_NAJECHANIA_NA_FIGURE);

        //----------------------------------------------------------------------
        //                  KOD LOGIKI
        //======================================================================
        //rozstawienie poczatkowe biale na gorze szachownicy, czarne na dole
        int wspKoloru;
        if (kolor_pionka == BIALE) {
            wspKoloru = 1;
        } else {
            wspKoloru = -1;
        }
        if (level == 0) {
            //ruch
            {
                if (FiguraNaPozycji(new Point(moja_pozycja.x - 1, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_BRAK) {
                    PokolorujPole(new Point(moja_pozycja.x - 1, moja_pozycja.y + 1 * wspKoloru), POLE_POSUNIECIA);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_BRAK) {
                    PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y + 1 * wspKoloru), POLE_POSUNIECIA);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x + 1, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_BRAK) {
                    PokolorujPole(new Point(moja_pozycja.x + 1, moja_pozycja.y + 1 * wspKoloru), POLE_POSUNIECIA);
                }
                if (FiguraNaPozycji(new Point(moja_pozycja.x - 1, moja_pozycja.y)) == FIGURA_BRAK) {
                    PokolorujPole(new Point(moja_pozycja.x - 1, moja_pozycja.y), POLE_POSUNIECIA);
                }
                if (FiguraNaPozycji(new Point(moja_pozycja.x + 1, moja_pozycja.y)) == FIGURA_BRAK) {
                    PokolorujPole(new Point(moja_pozycja.x + 1, moja_pozycja.y), POLE_POSUNIECIA);
                }
                if (FiguraNaPozycji(new Point(moja_pozycja.x - 1, moja_pozycja.y - 1 * wspKoloru)) == FIGURA_BRAK) {
                    PokolorujPole(new Point(moja_pozycja.x - 1, moja_pozycja.y - 1 * wspKoloru), POLE_POSUNIECIA);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y - 1 * wspKoloru)) == FIGURA_BRAK) {
                    PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y - 1 * wspKoloru), POLE_POSUNIECIA);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x + 1, moja_pozycja.y - 1 * wspKoloru)) == FIGURA_BRAK) {
                    PokolorujPole(new Point(moja_pozycja.x + 1, moja_pozycja.y - 1 * wspKoloru), POLE_POSUNIECIA);
                }
            }

            //bicie bezposrednie
            {
                if (FiguraNaPozycji(new Point(moja_pozycja.x - 1, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x - 1, moja_pozycja.y + 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y + 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x + 1, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x + 1, moja_pozycja.y + 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                }
                if (FiguraNaPozycji(new Point(moja_pozycja.x - 1, moja_pozycja.y)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x - 1, moja_pozycja.y), POLE_ATAKU_BEZPOSREDNIEGO);
                }
                if (FiguraNaPozycji(new Point(moja_pozycja.x + 1, moja_pozycja.y)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x + 1, moja_pozycja.y), POLE_ATAKU_BEZPOSREDNIEGO);
                }
                if (FiguraNaPozycji(new Point(moja_pozycja.x - 1, moja_pozycja.y - 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x - 1, moja_pozycja.y - 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y - 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y - 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x + 1, moja_pozycja.y - 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x + 1, moja_pozycja.y - 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x - 1, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x - 1, moja_pozycja.y + 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y + 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x + 1, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x + 1, moja_pozycja.y + 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                }
            }

        }
        if (level == 1) {
            //ruch
            {
                if (FiguraNaPozycji(new Point(moja_pozycja.x - 1, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_BRAK) {
                    PokolorujPole(new Point(moja_pozycja.x - 1, moja_pozycja.y + 1 * wspKoloru), POLE_POSUNIECIA);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_BRAK) {
                    PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y + 1 * wspKoloru), POLE_POSUNIECIA);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x + 1, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_BRAK) {
                    PokolorujPole(new Point(moja_pozycja.x + 1, moja_pozycja.y + 1 * wspKoloru), POLE_POSUNIECIA);
                }
                if (FiguraNaPozycji(new Point(moja_pozycja.x - 1, moja_pozycja.y)) == FIGURA_BRAK) {
                    PokolorujPole(new Point(moja_pozycja.x - 1, moja_pozycja.y), POLE_POSUNIECIA);
                }
                if (FiguraNaPozycji(new Point(moja_pozycja.x + 1, moja_pozycja.y)) == FIGURA_BRAK) {
                    PokolorujPole(new Point(moja_pozycja.x + 1, moja_pozycja.y), POLE_POSUNIECIA);
                }
                if (FiguraNaPozycji(new Point(moja_pozycja.x - 1, moja_pozycja.y - 1 * wspKoloru)) == FIGURA_BRAK) {
                    PokolorujPole(new Point(moja_pozycja.x - 1, moja_pozycja.y - 1 * wspKoloru), POLE_POSUNIECIA);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y - 1 * wspKoloru)) == FIGURA_BRAK) {
                    PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y - 1 * wspKoloru), POLE_POSUNIECIA);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x + 1, moja_pozycja.y - 1 * wspKoloru)) == FIGURA_BRAK) {
                    PokolorujPole(new Point(moja_pozycja.x + 1, moja_pozycja.y - 1 * wspKoloru), POLE_POSUNIECIA);
                }
            }

            //bicie bezposrednie
            {
                if (FiguraNaPozycji(new Point(moja_pozycja.x - 1, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x - 1, moja_pozycja.y + 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y + 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x + 1, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x + 1, moja_pozycja.y + 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                }
                if (FiguraNaPozycji(new Point(moja_pozycja.x - 1, moja_pozycja.y)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x - 1, moja_pozycja.y), POLE_ATAKU_BEZPOSREDNIEGO);
                }
                if (FiguraNaPozycji(new Point(moja_pozycja.x + 1, moja_pozycja.y)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x + 1, moja_pozycja.y), POLE_ATAKU_BEZPOSREDNIEGO);
                }
                if (FiguraNaPozycji(new Point(moja_pozycja.x - 1, moja_pozycja.y - 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x - 1, moja_pozycja.y - 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y - 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y - 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x + 1, moja_pozycja.y - 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x + 1, moja_pozycja.y - 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x - 1, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x - 1, moja_pozycja.y + 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y + 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x + 1, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x + 1, moja_pozycja.y + 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                }
            }
        }
    }
      
     @Override
    protected void odegraj_dziwek(int dzwiek) {
        //odgrywa dzwiek w zaleznosci od tego co zostalo zrobione
        switch (dzwiek) {

            case POLE_NIEZAZNACZONE://nidozwolona pozycja
                muzyka.graj("C:\\szachy\\dz_hydra_idz.wav", 1);
                break;
            case POLE_POSUNIECIA://przesunieto
                muzyka.graj("C:\\szachy\\dz_hydra_idz.wav", 1);
                break;
            case POLE_ATAKU_BEZPOSREDNIEGO://atak
                muzyka.graj("C:\\szachy\\dz_hydra_atak.wav", 1);
                break;
            case POLE_ATAKU_POSREDNIEGO://atak
                muzyka.graj("C:\\szachy\\dz_hydra_atak.wav", 1);
                break;
            case DZWIEK_NAJECHANIA_NA_FIGURE://najechanie myszka
                muzyka.graj("C:\\szachy\\dz_najechanie.wav", 1);
                break;
        }
    }
}
