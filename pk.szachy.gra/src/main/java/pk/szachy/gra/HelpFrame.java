/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.szachy.gra;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import java.awt.*;

/**
 *
 * @author Frogy07
 */
public class HelpFrame extends JFrame {

    private final JPanel textPanel;
    private JTextArea lvl1, lvl2;
    private ImageIcon Pic1, Pic2;
    private JButton button1, button2;
    private JComboBox kombobox;

    public HelpFrame() {
        textPanel = new JPanel();
        textPanel.setLayout(new BorderLayout());

        lvl1 = new JTextArea();
        lvl2 = new JTextArea();

        lvl1.setLineWrap(true);
        lvl2.setLineWrap(true);

        button1 = new JButton(Pic1);
        button2 = new JButton(Pic2);

        button1.setEnabled(false);
        button2.setEnabled(false);

        button1.setBackground(Color.DARK_GRAY);
        button2.setBackground(Color.DARK_GRAY);

        kombobox = new JComboBox();

        kombobox.addItem("Pionek");
        kombobox.addItem("Wieza");
        kombobox.addItem("Skoczek");
        kombobox.addItem("Goniec");
        kombobox.addItem("Krolowa");
        kombobox.addItem("Krol");
        kombobox.addItem("Armata");
        kombobox.addItem("Hydra");
        kombobox.addItem("Smok");

        button1.setBackground(Color.decode("#863C1C"));
        button2.setBackground(Color.decode("#863C1C"));
        Font font = new Font("Verdana", Font.BOLD, 12);
        lvl1.setFont(font);
        lvl2.setFont(font);
        lvl1.setBackground(Color.decode("#FFF5B6"));
        lvl2.setBackground(Color.decode("#FFF5B6"));
        
        //ustawienia layoutu
        add(kombobox, BorderLayout.NORTH);
        add(button1, BorderLayout.WEST);
        add(textPanel, BorderLayout.CENTER);
        textPanel.add(lvl1, BorderLayout.NORTH);
        textPanel.add(lvl2, BorderLayout.CENTER);
        add(button2, BorderLayout.EAST);

        //wybranie opcji z combobox
        kombobox.addActionListener(
                new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {

                        switch (kombobox.getSelectedIndex()) {
                            case 0: {
                                Pic1 = new ImageIcon("C:\\szachy\\pionek_b.png");
                                Pic2 = new ImageIcon("C:\\szachy\\pionek2_b.png");
                                lvl1.setText("Pionek porusza się do przodu. Bije tylko po skosie do przodu");
                                lvl2.setText("Pionek porusza się do niecofająco. Bije  po skosie w obu kierunkach");
                            }
                            break;
                            case 1: {
                                Pic1 = new ImageIcon("C:\\szachy\\wieza_b.png");
                                Pic2 = new ImageIcon("C:\\szachy\\wieza2_b.png");
                                lvl1.setText("Wieża porusza się pionowo i poziomo o 4 pola");
                                lvl2.setText("Czogł porusza się pionowo i poziomo o 4 pola ale strzela na pola 5-6");
                            }
                            break;
                            case 2: {
                                Pic1 = new ImageIcon("C:\\szachy\\konik_b.png");
                                Pic2 = new ImageIcon("C:\\szachy\\konik2_b.png");
                                lvl1.setText("Konik skacze klasycznie 2+1");
                                lvl2.setText("Husarz porusza się 3+1");
                            }
                            break;
                            case 3: {
                                Pic1 = new ImageIcon("C:\\szachy\\goniec_b.png");
                                Pic2 = new ImageIcon("C:\\szachy\\goniec2_b.png");
                                lvl1.setText("Goniec porusza się po skosie o 4 pola");
                                lvl2.setText("Asasyn porusza się po skosie o 6 pól");
                            }
                            break;
                            case 4: {
                                Pic1 = new ImageIcon("C:\\szachy\\krolowa_b.png");
                                Pic2 = new ImageIcon("C:\\szachy\\krolowa2_b.png");
                                lvl1.setText("Królowa porusza się pionowo, poziomo oraz po skosie o 5 pól");
                                lvl2.setText("Wiedźma porusza się pionowo, poziomo oraz po skosie o 7 pól");

                            }
                            break;
                            case 5: {
                                Pic1 = new ImageIcon("C:\\szachy\\krol_b.png");
                                Pic2 = new ImageIcon("C:\\szachy\\krol_b.png");
                                lvl1.setText("Król porusza się o jedno pole w każdą stronę");
                                lvl2.setText("");
                            }
                            break;
                            case 6: {
                                Pic1 = new ImageIcon("C:\\szachy\\armata_b.png");
                                Pic2 = new ImageIcon("C:\\szachy\\armata2_b.png");
                                lvl1.setText("Armata porusza się o 1 do przodu a strzela na 2 pola do przodu");
                                lvl2.setText("Moździerz porusza się o 1 do przodu a strzela na 2-3 pola do przodu");
                            }
                            break;
                            case 7: {
                                Pic1 = new ImageIcon("C:\\szachy\\hydra_b.png");
                                Pic2 = new ImageIcon("C:\\szachy\\hydra2_b.png");
                                lvl1.setText("Hydra porusza się o 1 w każdą stronę. Atakuje 3 wrogów na raz");
                                lvl2.setText("Hydra Chaosu porusza się o 1 w każdą stronę. Atakuje wszystkich wokół siebie");
                            }
                            break;
                            case 8: {
                                Pic1 = new ImageIcon("C:\\szachy\\smok_b.png");
                                Pic2 = new ImageIcon("C:\\szachy\\smok2_b.png");
                                lvl1.setText("Smok porusza się w każdą stronę o 5 pól ale atakuje tylko na 4");
                                lvl2.setText("Drakolicz porusza się w każdą stronę o 7 pól ale atakuje tylko na 6. Zieje ogniem po skosie");
                            }
                            break;

                        }

                        lvl1.setText("\n\n\nLEVEL: 1\n\n" + lvl1.getText() + "\n\n\n");
                        button1.setIcon(Pic1);
                        lvl2.setText("LEVEL: 2\n\n" + lvl2.getText() + "\n\n\n");
                        button1.setIcon(Pic1);
                        button2.setIcon(Pic2);
                        button1.setDisabledIcon(Pic1);
                        button2.setDisabledIcon(Pic2);
                    }
                }
        );
        pack();
        kombobox.setSelectedIndex(0);
    }

}
