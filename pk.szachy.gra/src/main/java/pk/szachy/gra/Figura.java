/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.szachy.gra;
import pk.szachy.grafika.IGrafika;
import pk.komponent.dzwiek.IDzwiek;

import java.awt.Color;
import java.awt.Point;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Kamil
 */
public abstract class Figura implements IFigura {

    //długosc i szerokosc obrazka na ekranie(do skalowania)
    public static int rozmiarObrazka = 57;
    public static int rozmiarSzachownicy = 8;

    public static final boolean BIALE = true;
    public static final boolean CZARNE = false;
    protected IGrafika grafika;
    String obrazek_lvl1;
    String obrazek_lvl2;
    //pozycja na szachownicy
    protected Point moja_pozycja;
    protected int nr_w_KompGraf;
    protected boolean kolor_pionka;
    protected int level;

    public static IDzwiek muzyka;
    public static boolean tura_koloru = BIALE;
    protected static List<Integer> lista_pokolorowanych_pol = new ArrayList<>();
    protected static List<Integer> lista_pionkow = new ArrayList<>();
    public final static int POLE_NIEZAZNACZONE = -1;
    public final static int POLE_POSUNIECIA = 0;
    public final static int POLE_ATAKU_BEZPOSREDNIEGO = 1;
    public final static int POLE_ATAKU_POSREDNIEGO = 2;
    public final static int DZWIEK_NAJECHANIA_NA_FIGURE = 3;
    public final static int FIGURA_PRZECIWNIKA = 10;
    public final static int FIGURA_SWOICH = 11;
    public final static int FIGURA_BRAK = 12;

    public Figura(IGrafika grafika, String sciezkaPlikuGraficznego_lvl1, String sciezkaPlikuGraficznego_lvl2, boolean kolor, int poz_x, int poz_y) {

        this.grafika = grafika;

        level = 0;
        obrazek_lvl2 = sciezkaPlikuGraficznego_lvl2;
        this.kolor_pionka = kolor;

        moja_pozycja = new Point(poz_x, poz_y);

        dodajMnieDoKomponentuGraficznego(sciezkaPlikuGraficznego_lvl1);
        lista_pionkow.add(nr_w_KompGraf);
    }

    private void dodajMnieDoKomponentuGraficznego(String sciezkaPlikuGraficznego) {
        nr_w_KompGraf = grafika.dodajGrafike(rozmiarObrazka * moja_pozycja.x, rozmiarObrazka * moja_pozycja.y, sciezkaPlikuGraficznego, IGrafika.PRZENOS, this);
        grafika.ustawRozmiarGrafiki(nr_w_KompGraf, rozmiarObrazka, rozmiarObrazka);

        try {
            //dodawanie funkcji obslugi zdarzen myszki
            Method method;

            method = Figura.class.getMethod("najechanoNaMnie", new Class[]{});
            grafika.dodajFuncjeNajechanoNaObiekt(nr_w_KompGraf, method);

            method = Figura.class.getMethod("zjechanoZeMnie", new Class[]{});
            grafika.dodajFuncjeZjechanoZObiektu(nr_w_KompGraf, method);

            method = Figura.class.getMethod("przeniesionoMnie", new Class[]{int.class, int.class, int.class, int.class});
            grafika.dodajFuncjePrzesunietoObiekt(nr_w_KompGraf, method);

            method = Figura.class.getMethod("podniesionoMnie", new Class[]{});
            grafika.dodajFuncjeKliknieciaNaObiekt(nr_w_KompGraf, method);

        } catch (NoSuchMethodException | SecurityException ex) {
            Logger.getAnonymousLogger().log(Level.WARNING, "podano niewlasciwa nazwe metody");
        }
    }

    private void zbijFigurePodeMna() {
        //nie moze byc wartwy kolorow
        przywrocKoloryPol();

        //sprawdzamy co znajduje sie pod pionkiem
        grafika.ustawWidocznosc(nr_w_KompGraf, false);
        int nr_grafiki_pod_pionkiem = grafika.podajGrafikeNaPozycji(moja_pozycja.x * rozmiarObrazka + 30, moja_pozycja.y * rozmiarObrazka + 30);
        grafika.ustawWidocznosc(nr_w_KompGraf, true);

        //jesli zbilismy krola 
        if ((grafika.podajObjekt(nr_grafiki_pod_pionkiem) instanceof Krol)) {
            muzyka.graj("C:\\szachy\\dz_koniec_gry.wav", 1);
            JOptionPane.showMessageDialog(null, "Zabito króla", "Koniec Gry", JOptionPane.INFORMATION_MESSAGE);
            System.exit(0);
        }

        //jesli jest tam inna figura usuwamy ja 
        if ((grafika.podajObjekt(nr_grafiki_pod_pionkiem) instanceof Figura)) {
            level = 1;
            grafika.zmienObrazGrafiki(nr_w_KompGraf, obrazek_lvl2);
            grafika.ustawRozmiarGrafiki(nr_w_KompGraf, rozmiarObrazka, rozmiarObrazka);
            grafika.usunGrafike(nr_grafiki_pod_pionkiem);

            for (Iterator<Integer> iter = lista_pionkow.listIterator(); iter.hasNext();) {
                int a = iter.next();
                if (a == nr_grafiki_pod_pionkiem) {
                    iter.remove();
                }
            }
        }
    }

    private void przywrocKoloryPol() {

        lista_pokolorowanych_pol.stream().forEach((item) -> {
            grafika.usunGrafike(item);
        });
        lista_pokolorowanych_pol.clear();
    }

    //stawia pionek przeliczajac jego nowa pozycje
    ///@param pozycja pozycja w notacji szachowej
    private void ustawMnieNaNowejPozycji(Point pozycja) {
        grafika.przenies(nr_w_KompGraf, pozycja.x * rozmiarObrazka, pozycja.y * rozmiarObrazka);
        moja_pozycja = pozycja;
    }

    ///@breif Zamienia wspolrzedne z pikseli na notacje szachowa
    ///@param graf_x pozycja x w pikselach
    ///@param graf_y pozycja y w pikselach
    private Point wspGrafnaTabl(int graf_x, int graf_y) {
        return new Point(((graf_x + 30) / rozmiarObrazka), ((graf_y + 30) / rozmiarObrazka));
    }

    ///@breif Metoda przesuwa wszystkie grafiki pionkow na wierz ekranu, zapobiega to zaslonieciu figury gdy kolorujemy pole na ktorym stoi figura 
    protected void przesunWszystkiePionkiNaWierzch() {
        lista_pionkow.stream().forEach((item) -> {
            grafika.przesunNaWierzch(item);
        });
    }

    /**
     * @brief sprawdza na podstawie kolorow szachownicy czy dane pole jest polem
     * ataku, ruchu lub nie jest zaznaczone (zwraca info o nieznanzaczonym polu
     * jesli wyjdzie poza tablice)
     * @param pole pole do sprawdzenia (ma byc relatywne)
     */
    private int sprawdz_kolor_pola(Point pole) {

        int ret = POLE_NIEZAZNACZONE;
        grafika.ustawWidocznosc(nr_w_KompGraf, false);
        //tu moze byc pionek, zaznaczenie(grafika z kolorem) lub zwykle pole szachowe
        int nr_grafiki_pod_pionkiem_1 = grafika.podajGrafikeNaPozycji(pole.x * rozmiarObrazka + 30, pole.y * rozmiarObrazka + 30);
        grafika.ustawWidocznosc(nr_grafiki_pod_pionkiem_1, false);

        //tu moze byc albo zaznaczenie albo pole szachowe
        int nr_grafiki_pod_pionkiem_2 = grafika.podajGrafikeNaPozycji(pole.x * rozmiarObrazka + 30, pole.y * rozmiarObrazka + 30);

        //czy na 2 warswie w glab jest zaznaczenie?
        if (nr_grafiki_pod_pionkiem_2 != -1 && (grafika.podajObjekt(nr_grafiki_pod_pionkiem_2) instanceof Integer) == true) {
            ret = (int) grafika.podajObjekt(nr_grafiki_pod_pionkiem_2);
        } else if (nr_grafiki_pod_pionkiem_1 != -1 && (grafika.podajObjekt(nr_grafiki_pod_pionkiem_1) instanceof Integer) == true) {
            ret = (int) grafika.podajObjekt(nr_grafiki_pod_pionkiem_1);
        }

        grafika.ustawWidocznosc(nr_w_KompGraf, true);
        grafika.ustawWidocznosc(nr_grafiki_pod_pionkiem_1, true);
        return ret;
    }

    @Override
    public void przeniesionoMnie(int poz_stara_x, int poz_stara_y, int poz_nowa_x, int poz_nowa_y) {
        //zamieniamy wsp graficzne na notacje szachowa
        Point wspNowa = wspGrafnaTabl(poz_nowa_x, poz_nowa_y);
        Point wspStara = wspGrafnaTabl(poz_stara_x, poz_stara_y);

        switch (sprawdz_kolor_pola(wspNowa)) {
            case POLE_NIEZAZNACZONE:
                //no to wracamy skad nas przeniesiono
                //////////////////////////////////////////////////////////////tutaj zamienic po testach
                ustawMnieNaNowejPozycji(wspStara);

                odegraj_dziwek(POLE_NIEZAZNACZONE);

                //uwaga return! aby nie zmienialo tury gdy pionek sie nie poruszy
                return;
            case POLE_ATAKU_BEZPOSREDNIEGO:
                odegraj_dziwek(POLE_ATAKU_BEZPOSREDNIEGO);
                //wskakujemy na nowe pole i bijemy
                ustawMnieNaNowejPozycji(wspNowa);
                zbijFigurePodeMna();
                break;
            case POLE_POSUNIECIA:
                //wskakujemy na nowe pole
                ustawMnieNaNowejPozycji(wspNowa);
                odegraj_dziwek(POLE_POSUNIECIA);
                break;
            case POLE_ATAKU_POSREDNIEGO:
                odegraj_dziwek(POLE_ATAKU_POSREDNIEGO);
                //wskakujemy na pole ktore atakujemy, bijemy piona i wracamy na skad strzelalismy
                ustawMnieNaNowejPozycji(wspNowa);
                zbijFigurePodeMna();
                ustawMnieNaNowejPozycji(wspStara);
                break;
        }

        //aby szachownica miala normalne kolory
        przywrocKoloryPol();

        //zamiana tury
        tura_koloru = !tura_koloru;
    }

    @Override
    public void zjechanoZeMnie() {
        przywrocKoloryPol();
    }

    @Override
    public void podniesionoMnie() {
        //niepotrzebne
    }

    ///@param pozycja w notacji szchowej
    ///@return zwraca jedna ze stalych FIGURA_BRAK, FIGURA_PRZECIWNIKA lub FIGURA_SWOICH
    protected int FiguraNaPozycji(Point pozycja) {
        int nrSprGrafiki = grafika.podajGrafikeNaPozycji(pozycja.x * rozmiarObrazka + 30, pozycja.y * rozmiarObrazka + 30);
        Object obj = grafika.podajObjekt(nrSprGrafiki);
        if ((obj instanceof Figura)) {
            Figura fig = (Figura) obj;
            if (fig.kolor_pionka != this.kolor_pionka) {
                return FIGURA_PRZECIWNIKA;
            } else {
                return FIGURA_SWOICH;
            }
        } else {
            return FIGURA_BRAK;
        }
    }

    protected void PokolorujPole(Point Pozycja, int rodzaj_pola) {
        //policz czy pole miesci sie na szachownicy
        if (Pozycja.x < 0 || Pozycja.x > rozmiarSzachownicy - 1 || Pozycja.y < 0 || Pozycja.y > rozmiarSzachownicy - 1) {
            return;
        }

        int nr = -1;
        if (rodzaj_pola == POLE_ATAKU_BEZPOSREDNIEGO) {
            nr = grafika.dodajGrafike(Pozycja.x * rozmiarObrazka, Pozycja.y * rozmiarObrazka, "C:\\szachy\\red.png", false, POLE_ATAKU_BEZPOSREDNIEGO);
        }
        if (rodzaj_pola == POLE_POSUNIECIA) {
            nr = grafika.dodajGrafike(Pozycja.x * rozmiarObrazka, Pozycja.y * rozmiarObrazka, "C:\\szachy\\green.png", false, POLE_POSUNIECIA);
        }
        if (rodzaj_pola == POLE_ATAKU_POSREDNIEGO) {
            nr = grafika.dodajGrafike(Pozycja.x * rozmiarObrazka, Pozycja.y * rozmiarObrazka, "C:\\szachy\\yellow.png", false, POLE_ATAKU_POSREDNIEGO);
        }

        grafika.ustawRozmiarGrafiki(nr, rozmiarObrazka, rozmiarObrazka);
        lista_pokolorowanych_pol.add(nr);
        przesunWszystkiePionkiNaWierzch();
    }

    protected void odegraj_dziwek(int dzwiek) {
        //odgrywa dzwiek w zaleznosci od tego co zostalo zrobione
        switch (dzwiek) {

            case POLE_NIEZAZNACZONE://nidozwolona pozycja
                muzyka.graj("C:\\szachy\\dz_idz.wav", 1);
                break;
            case POLE_POSUNIECIA://przesunieto
                muzyka.graj("C:\\szachy\\dz_idz.wav", 1);
                break;
            case POLE_ATAKU_BEZPOSREDNIEGO://atak
                muzyka.graj("C:\\szachy\\dz_atak_blisko.wav", 1);
                break;
            case POLE_ATAKU_POSREDNIEGO://atak
                muzyka.graj("C:\\szachy\\dz_atak_daleko.wav", 1);
                break;
            case DZWIEK_NAJECHANIA_NA_FIGURE://najechanie myszka
                muzyka.graj("C:\\szachy\\dz_najechanie.wav", 1);
                break;
        }
    }
}
