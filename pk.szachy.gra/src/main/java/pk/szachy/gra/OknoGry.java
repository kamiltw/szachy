package pk.szachy.gra;

import pk.szachy.grafika.IGrafika;
import pk.komponent.dzwiek.IDzwiek;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;

/**
 *
 * @author Kamil
 */
public class OknoGry {

    private IGrafika grafika;
    private IDzwiek dzwiek;
    private BundleContext context;

    private JFrame frame;
    private Szachownica szachownica;
    JLabel textBiali;
    JLabel textCzarni;

    public OknoGry(BundleContext con, IDzwiek dzw, IGrafika graf) {

        grafika = graf;
        dzwiek = dzw;
        context = con;

        start();
    }

    public void start() {
        frame = new JFrame("Szachy OSGI");

        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                try {
                    // w
                    context.getBundle(0).stop();
                } catch (BundleException ex) {
                    ex.printStackTrace();
                }
            }
        });

        frame.setSize(720, 630);
        frame.setVisible(true);
        frame.setLayout(null);
        frame.setLocation(300, 100);
        frame.setResizable(false);

        szachownica = new Szachownica(grafika, dzwiek);

        JButton buttonPomoc = new JButton("pomoc");
        buttonPomoc.setBackground(Color.yellow);

        JPanel paneliknabutonik = new JPanel();
        paneliknabutonik.add(buttonPomoc);
        JPanel panelInfo = new JPanel();
        panelInfo.setLayout(new GridLayout(10, 1));
        panelInfo.add(buttonPomoc);
        panelInfo.setBackground(Color.DARK_GRAY);
        panelInfo.add(Box.createHorizontalGlue());
        panelInfo.add(Box.createHorizontalGlue());
        panelInfo.add(Box.createHorizontalGlue());
        panelInfo.add(Box.createHorizontalGlue());
        panelInfo.add(Box.createHorizontalGlue());
        panelInfo.add(Box.createHorizontalGlue());

        buttonPomoc.addActionListener((ActionEvent e) -> {
            szachownica.pomoc();
        });

        frame.setLayout(new BorderLayout());
        frame.add(grafika.podajPanel());
        frame.add(panelInfo, BorderLayout.EAST);

        frame.validate();
        frame.repaint();

    }

}
