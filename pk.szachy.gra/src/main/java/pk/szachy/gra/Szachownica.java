/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.szachy.gra;
import pk.komponent.dzwiek.IDzwiek;
import pk.szachy.grafika.IGrafika;

import java.awt.Color;
import java.awt.Point;

/**
 *
 * @author Kamil
 */
public class Szachownica {

    public static int rozmiarObrazka = 57;
    public static int rozmiarSzachownicy = 10; 
    IGrafika grafika;
    HelpFrame pomoc;
    IDzwiek muzyka;

    public Szachownica(IGrafika grafika, IDzwiek dzwiek) {
        this.grafika = grafika;
        grafika.ustawTlo(Color.darkGray);
        Figura.rozmiarObrazka = rozmiarObrazka;
        Figura.rozmiarSzachownicy = rozmiarSzachownicy;
        grafika.skalowaniePodczasPrzenoszenia(1.2);
        NarysujStol();
        RozstawPionki();
        muzyka= dzwiek;
        Figura.muzyka=muzyka;
        muzyka.Grajzapetlone("C:\\szachy\\dz_muzyka.wav");
    }

    public void pomoc()
    {
        pomoc=new HelpFrame();
        pomoc.setVisible(true);
        pomoc.setSize(700,400);
        pomoc.setResizable(false);
        pomoc.setAlwaysOnTop(true);
        
    }
    
    private void NarysujStol() {
        for (int j = 0; j < 5; j++) { 
            for (int i = 0; i < 5; i++) { 
                grafika.dodajGrafike(rozmiarObrazka * i * 2, rozmiarObrazka * j * 2, "C:\\szachy\\b.png");
                grafika.dodajGrafike(rozmiarObrazka * i * 2 + rozmiarObrazka, rozmiarObrazka * j * 2, "C:\\szachy\\c.png");
                grafika.dodajGrafike(rozmiarObrazka * i * 2, rozmiarObrazka * j * 2 + rozmiarObrazka, "C:\\szachy\\c.png");
                grafika.dodajGrafike(rozmiarObrazka * i * 2 + rozmiarObrazka, rozmiarObrazka * j * 2 + rozmiarObrazka, "C:\\szachy\\b.png");
            }
        }

        for (int i = grafika.podajLiczbeGrafik() - rozmiarSzachownicy*rozmiarSzachownicy; i <= grafika.podajLiczbeGrafik(); i++) {
            grafika.ustawRozmiarGrafiki(i, rozmiarObrazka, rozmiarObrazka);
        }

    }

    private void RozstawPionki() {

        new Wieza(grafika, "C:\\szachy\\wieza_b.png", "C:\\szachy\\wieza2_b.png", Figura.BIALE, 0, 0);
        new Hydra(grafika, "C:\\szachy\\hydra_b.png", "C:\\szachy\\hydra2_b.png", Figura.BIALE, 3, 0);
        new Skoczek(grafika, "C:\\szachy\\konik_b.png", "C:\\szachy\\konik2_b.png", Figura.BIALE, 1, 0);       
        new Goniec(grafika, "C:\\szachy\\goniec_b.png", "C:\\szachy\\goniec2_b.png", Figura.BIALE, 2, 0);
        new Krolowa(grafika, "C:\\szachy\\krolowa_b.png", "C:\\szachy\\krolowa2_b.png", Figura.BIALE, 4, 0);  //Zmiana pozycji białego króla z królową
        new Krol(grafika, "C:\\szachy\\krol_b.png", "C:\\szachy\\krol_b.png", Figura.BIALE, 5, 0);
        new Goniec(grafika, "C:\\szachy\\goniec_b.png", "C:\\szachy\\goniec2_b.png", Figura.BIALE, 7, 0);
        new Hydra(grafika, "C:\\szachy\\hydra_b.png", "C:\\szachy\\hydra2_b.png", Figura.BIALE, 6, 0);
        new Skoczek(grafika, "C:\\szachy\\konik_b.png", "C:\\szachy\\konik2_b.png", Figura.BIALE, 8, 0);       
        new Wieza(grafika, "C:\\szachy\\wieza_b.png", "C:\\szachy\\wieza2_b.png", Figura.BIALE, 9, 0);
       // 
        new Wieza(grafika, "C:\\szachy\\wieza_b.png", "C:\\szachy\\wieza2_b.png", Figura.BIALE, 1, 1);
        new Smok(grafika, "C:\\szachy\\smok_b.png", "C:\\szachy\\smok2_b.png", Figura.BIALE, 0, 1);
        new Skoczek(grafika, "C:\\szachy\\konik_b.png", "C:\\szachy\\konik2_b.png", Figura.BIALE, 3, 1);       
        new Goniec(grafika, "C:\\szachy\\goniec_b.png", "C:\\szachy\\goniec2_b.png", Figura.BIALE, 2, 1); 
        
        new Wieza(grafika, "C:\\szachy\\wieza_b.png", "C:\\szachy\\wieza2_b.png", Figura.BIALE, 8, 1);
        new Smok(grafika, "C:\\szachy\\smok_b.png", "C:\\szachy\\smok2_b.png", Figura.BIALE, 9, 1);
        new Skoczek(grafika, "C:\\szachy\\konik_b.png", "C:\\szachy\\konik2_b.png", Figura.BIALE, 6, 1);       
        new Goniec(grafika, "C:\\szachy\\goniec_b.png", "C:\\szachy\\goniec2_b.png", Figura.BIALE, 7, 1);
        
        new Armata(grafika, "C:\\szachy\\armata_b.png", "C:\\szachy\\armata2_b.png", Figura.BIALE, 4, 1);       
        new Armata(grafika, "C:\\szachy\\armata_b.png", "C:\\szachy\\armata2_b.png", Figura.BIALE, 5, 1); 
       // new Armata(grafika, "C:\\szachy\\armata_b.png", Figura.BIALE, 6, 0);*/
        
        
        new Wieza(grafika, "C:\\szachy\\wieza_c.png", "C:\\szachy\\wieza_c.png", Figura.CZARNE, 0, 9);
        new Hydra(grafika, "C:\\szachy\\hydra_c.png", "C:\\szachy\\hydra2_c.png", Figura.CZARNE, 3, 9);
        new Skoczek(grafika, "C:\\szachy\\konik_c.png", "C:\\szachy\\konik_c.png", Figura.CZARNE, 1, 9);
        new Goniec(grafika, "C:\\szachy\\goniec_c.png", "C:\\szachy\\goniec_c.png", Figura.CZARNE, 2, 9);
        new Krolowa(grafika, "C:\\szachy\\krolowa_c.png", "C:\\szachy\\krolowa_c.png", Figura.CZARNE, 4, 9);
        new Krol(grafika, "C:\\szachy\\krol_c.png", "C:\\szachy\\krol_c.png", Figura.CZARNE, 5, 9);
        
       // new Armata(grafika, "C:\\szachy\\armata_c.png", Figura.CZARNE, 6, 7);
        new Goniec(grafika, "C:\\szachy\\goniec_c.png", "C:\\szachy\\goniec2_c.png", Figura.CZARNE, 7, 9);
        new Hydra(grafika, "C:\\szachy\\hydra_c.png", "C:\\szachy\\hydra2_c.png", Figura.CZARNE, 6, 9);
        new Skoczek(grafika, "C:\\szachy\\konik_c.png", "C:\\szachy\\konik2_c.png", Figura.CZARNE, 8, 9);
        new Wieza(grafika, "C:\\szachy\\wieza_c.png", "C:\\szachy\\wieza2_c.png", Figura.CZARNE, 9, 9);
        
        new Wieza(grafika, "C:\\szachy\\wieza_c.png", "C:\\szachy\\wieza2_c.png", Figura.CZARNE, 1, 8);
        new Smok(grafika, "C:\\szachy\\smok_c.png", "C:\\szachy\\smok2_c.png", Figura.CZARNE, 0, 8);
        new Skoczek(grafika, "C:\\szachy\\konik_c.png", "C:\\szachy\\konik2_c.png", Figura.CZARNE, 3, 8);       
        new Goniec(grafika, "C:\\szachy\\goniec_c.png", "C:\\szachy\\goniec2_c.png", Figura.CZARNE, 2, 8); 
        
        new Wieza(grafika, "C:\\szachy\\wieza_c.png", "C:\\szachy\\wieza2_c.png", Figura.CZARNE, 8, 8);
        new Smok(grafika, "C:\\szachy\\smok_c.png", "C:\\szachy\\smok2_c.png", Figura.CZARNE, 9, 8);
        new Skoczek(grafika, "C:\\szachy\\konik_c.png", "C:\\szachy\\konik2_c.png", Figura.CZARNE, 6, 8);       
        new Goniec(grafika, "C:\\szachy\\goniec_c.png", "C:\\szachy\\goniec_2c.png", Figura.CZARNE, 7, 8);
        
        new Armata(grafika, "C:\\szachy\\armata_c.png", "C:\\szachy\\armata2_c.png", Figura.CZARNE, 4, 8);       
        new Armata(grafika, "C:\\szachy\\armata_c.png", "C:\\szachy\\armata2_c.png", Figura.CZARNE, 5, 8); 
        
        
        for (int i = 0; i < 10; i++) //Dodaje same bierki
        {
        new Pionek(grafika, "C:\\szachy\\pionek_b.png", "C:\\szachy\\pionek2_b.png", Figura.BIALE, i, 2);
        new Pionek(grafika, "C:\\szachy\\pionek_c.png", "C:\\szachy\\pionek2_c.png", Figura.CZARNE, i, 7);
        }

    }

    private class PoleSzachowe {

        Point pozycja;

        public Point getPozycja() {
            return pozycja;
        }

        public PoleSzachowe(Point pozycja) {
            this.pozycja = pozycja;
        }

    }

}
