package pk.szachy.gra.internal;

import pk.komponent.dzwiek.IDzwiek;
import pk.szachy.grafika.IGrafika;
import pk.szachy.gra.OknoGry;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;

import javax.swing.JOptionPane;
import org.osgi.framework.ServiceReference;

/**
 * Glowna paczka z gra
 */
public class Activator implements BundleActivator, Runnable {

    private static BundleContext context;
    private IGrafika grafika;
    private IDzwiek dzwiek;
    private OknoGry oknoGry;

    static BundleContext getContext() {
        return context;
    }

    public void start(BundleContext bundleContext) throws Exception {
        Activator.context = bundleContext;

        if (SwingUtilities.isEventDispatchThread()) {
            run();
        } else {
            try {
                javax.swing.SwingUtilities.invokeLater(this);
            } catch (Exception ex) {
                System.err.println("Cos poszlo nie tak");
                ex.printStackTrace();
            }
        }
    }

    public void stop(BundleContext bundleContext) throws Exception {
        Activator.context = null;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Activator.class.getName()).log(Level.SEVERE, null, ex);
        } 
        ServiceReference srDz = context.getServiceReference(IDzwiek.class.getName());
        ServiceReference srGr = context.getServiceReference(IGrafika.class.getName());

        if (srDz == null) {
            System.out.println("Brak dzwięku, dalej nie idę!");
            return;
        }

        if (srGr == null) {
            System.out.println("Brak grafiki, dalej nie idę!");
            return;
        }

        dzwiek = (IDzwiek) context.getService(srDz);
        grafika = (IGrafika) context.getService(srGr);

        oknoGry = new OknoGry(context, dzwiek, grafika);
    }
}
