/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.szachy.gra;
import pk.szachy.grafika.IGrafika;

import java.awt.Color;
import java.awt.Point;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kamil
 */
public class Pionek extends Figura {

    Pionek(IGrafika grafika, String sciezkaPlikuGraficznego_lvl1, String sciezkaPlikuGraficznego_lvl2, boolean kolor_bialy, int poz_x, int poz_y) {
        super(grafika, sciezkaPlikuGraficznego_lvl1, sciezkaPlikuGraficznego_lvl2, kolor_bialy, poz_x, poz_y);
    }

    //zaznacz kolorami pola posuniecia,ataku i ataku na odleglosc
    @Override
    public void najechanoNaMnie() {
        //sprawdz czy jest tura innego koloru
        //jesli tak wyjdz
        if (tura_koloru != this.kolor_pionka) {
            return;
        }

        odegraj_dziwek(DZWIEK_NAJECHANIA_NA_FIGURE);

        //----------------------------------------------------------------------
        //                  KOD LOGIKI
        //======================================================================
        //rozstawienie poczatkowe biale na gorze szachownicy, czarne na dole
        int wspKoloru;
        if (kolor_pionka == BIALE) {
            wspKoloru = 1;
        } else {
            wspKoloru = -1;
        }

        if (level == 0) {
            //ruch
            {
                if (FiguraNaPozycji(new Point(moja_pozycja.x - 1, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_BRAK) {
                    PokolorujPole(new Point(moja_pozycja.x - 1, moja_pozycja.y + 1 * wspKoloru), POLE_POSUNIECIA);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_BRAK) {
                    PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y + 1 * wspKoloru), POLE_POSUNIECIA);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x + 1, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_BRAK) {
                    PokolorujPole(new Point(moja_pozycja.x + 1, moja_pozycja.y + 1 * wspKoloru), POLE_POSUNIECIA);
                }
            }

            //bicie bezposrednie
            {
                if (FiguraNaPozycji(new Point(moja_pozycja.x - 1, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x - 1, moja_pozycja.y + 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                }


                /*  if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                 PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y + 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                 }
                 */
                if (FiguraNaPozycji(new Point(moja_pozycja.x + 1, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x + 1, moja_pozycja.y + 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);

                }

            }

            //bicie na odleglosc - BRAK
            /*
             {
             if (FiguraNaPozycji(new Point(moja_pozycja.x - 1, moja_pozycja.y + 2 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
             PokolorujPole(new Point(moja_pozycja.x - 1, moja_pozycja.y + 2 * wspKoloru), POLE_ATAKU_POSREDNIEGO);
             }

             if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y + 2 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
             PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y + 2 * wspKoloru), POLE_ATAKU_POSREDNIEGO);
             }

             if (FiguraNaPozycji(new Point(moja_pozycja.x + 1, moja_pozycja.y + 2 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
             PokolorujPole(new Point(moja_pozycja.x + 1, moja_pozycja.y + 2 * wspKoloru), POLE_ATAKU_POSREDNIEGO);
             }
             }*/
        }

        if (level == 1) {
            //ruch
            {
                if (FiguraNaPozycji(new Point(moja_pozycja.x - 1, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_BRAK) {
                    PokolorujPole(new Point(moja_pozycja.x - 1, moja_pozycja.y + 1 * wspKoloru), POLE_POSUNIECIA);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_BRAK) {
                    PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y + 1 * wspKoloru), POLE_POSUNIECIA);
                }

                if (FiguraNaPozycji(new Point(moja_pozycja.x + 1, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_BRAK) {
                    PokolorujPole(new Point(moja_pozycja.x + 1, moja_pozycja.y + 1 * wspKoloru), POLE_POSUNIECIA);
                }
                if (FiguraNaPozycji(new Point(moja_pozycja.x - 1, moja_pozycja.y)) == FIGURA_BRAK) {
                    PokolorujPole(new Point(moja_pozycja.x - 1, moja_pozycja.y), POLE_POSUNIECIA);
                }
                if (FiguraNaPozycji(new Point(moja_pozycja.x + 1, moja_pozycja.y)) == FIGURA_BRAK) {
                    PokolorujPole(new Point(moja_pozycja.x + 1, moja_pozycja.y), POLE_POSUNIECIA);
                }

            }

            //bicie bezposrednie
            {
                if (FiguraNaPozycji(new Point(moja_pozycja.x - 1, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x - 1, moja_pozycja.y + 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                }

                /* if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                 PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y + 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                 }*/
                if (FiguraNaPozycji(new Point(moja_pozycja.x + 1, moja_pozycja.y + 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x + 1, moja_pozycja.y + 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                }
                if (FiguraNaPozycji(new Point(moja_pozycja.x + 1, moja_pozycja.y - 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x + 1, moja_pozycja.y - 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);

                }
                if (FiguraNaPozycji(new Point(moja_pozycja.x - 1, moja_pozycja.y - 1 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
                    PokolorujPole(new Point(moja_pozycja.x - 1, moja_pozycja.y - 1 * wspKoloru), POLE_ATAKU_BEZPOSREDNIEGO);
                }
            }

            //bicie na odleglosc - brak
            /*
             {
             if (FiguraNaPozycji(new Point(moja_pozycja.x - 1, moja_pozycja.y + 2 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
             PokolorujPole(new Point(moja_pozycja.x - 1, moja_pozycja.y + 2 * wspKoloru), POLE_ATAKU_POSREDNIEGO);
             }

             if (FiguraNaPozycji(new Point(moja_pozycja.x, moja_pozycja.y + 2 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
             PokolorujPole(new Point(moja_pozycja.x, moja_pozycja.y + 2 * wspKoloru), POLE_ATAKU_POSREDNIEGO);
             }

             if (FiguraNaPozycji(new Point(moja_pozycja.x + 1, moja_pozycja.y + 2 * wspKoloru)) == FIGURA_PRZECIWNIKA) {
             PokolorujPole(new Point(moja_pozycja.x + 1, moja_pozycja.y + 2 * wspKoloru), POLE_ATAKU_POSREDNIEGO);
             }
             }*/
        }
    }
}
