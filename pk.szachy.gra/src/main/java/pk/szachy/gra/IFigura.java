/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.szachy.gra;

import pk.szachy.grafika.IGrafika;

/**
 *
 * @author Kamil
 */
public interface IFigura {
    
    
    public  void najechanoNaMnie();
    
    public void zjechanoZeMnie();
    
    public void przeniesionoMnie(int poz_stara_x,int poz_stara_y,int poz_nowa_x,int poz_nowa_y);
    
    public void podniesionoMnie();
 
}
