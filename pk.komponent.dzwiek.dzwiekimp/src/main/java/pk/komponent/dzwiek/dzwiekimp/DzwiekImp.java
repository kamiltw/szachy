/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.komponent.dzwiek.dzwiekimp;

import pk.komponent.dzwiek.IDzwiek;
import javax.sound.sampled.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author Loki
 */
public class DzwiekImp implements IDzwiek {

    //String sciezkaAudio = "../dzwiek/src/dzwiek/muzyka.wav";
    AudioInputStream sound;
    String sdz;

    @Override
    public void stop() {

    }

    @Override
    public void Grajzapetlone(String sciezka) {
        File soundFile = new File(sciezka);
        AudioInputStream sound = null;
        try {
            sound = AudioSystem.getAudioInputStream(soundFile);
        } catch (UnsupportedAudioFileException ex) {
            Logger.getLogger(DzwiekImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DzwiekImp.class.getName()).log(Level.SEVERE, null, ex);
        }

        // load the sound into memory (a Clip)
        DataLine.Info info = new DataLine.Info(Clip.class, sound.getFormat());
        Clip clip = null;
        try {
            clip = (Clip) AudioSystem.getLine(info);
        } catch (LineUnavailableException ex) {
            Logger.getLogger(DzwiekImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            clip.open(sound);
        } catch (LineUnavailableException ex) {
            Logger.getLogger(DzwiekImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DzwiekImp.class.getName()).log(Level.SEVERE, null, ex);
        }

        // due to bug in Java Sound, explicitly exit the VM when
        // the sound has stopped.
        clip.addLineListener(new LineListener() {
            public void update(LineEvent event) {
                if (event.getType() == LineEvent.Type.STOP) {
                    event.getLine().close();
                }
            }
        });

        // play the sound clip
        clip.setFramePosition(0);
        clip.loop(Clip.LOOP_CONTINUOUSLY);
        clip.start();

    }

    @Override
    public void graj(String sciezka, int ilosc_razy) {

        File soundFile = new File(sciezka);

        AudioInputStream sound = null;
        try {
            sound = AudioSystem.getAudioInputStream(soundFile);
            //sound = AudioSystem.getAudioInputStream(this.getClass().getResourceAsStream(sciezka));
        } catch (UnsupportedAudioFileException ex) {
            Logger.getLogger(DzwiekImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DzwiekImp.class.getName()).log(Level.SEVERE, null, ex);
        }

        // load the sound into memory (a Clip)
        DataLine.Info info = new DataLine.Info(Clip.class, sound.getFormat());
        Clip clip = null;
        try {
            clip = (Clip) AudioSystem.getLine(info);
        } catch (LineUnavailableException ex) {
            Logger.getLogger(DzwiekImp.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            clip.open(sound);
        } catch (LineUnavailableException ex) {
            Logger.getLogger(DzwiekImp.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(DzwiekImp.class.getName()).log(Level.SEVERE, null, ex);
        }

        // due to bug in Java Sound, explicitly exit the VM when
        // the sound has stopped.
        clip.addLineListener(new LineListener() {
            public void update(LineEvent event) {
                if (event.getType() == LineEvent.Type.STOP) {
                    event.getLine().close();
                }
            }
        });

        // play the sound clip
        clip.setFramePosition(0);
        clip.loop(ilosc_razy - 1);
        clip.start();
    }
}
