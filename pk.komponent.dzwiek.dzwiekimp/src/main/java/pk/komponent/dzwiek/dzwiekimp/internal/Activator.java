package pk.komponent.dzwiek.dzwiekimp.internal;

import java.util.Hashtable;

import javax.swing.ImageIcon;

import pk.komponent.dzwiek.IDzwiek;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import javax.swing.JOptionPane;
import pk.komponent.dzwiek.dzwiekimp.DzwiekImp;

/**
 * Serwis Dzwieku
 **/

public class Activator implements BundleActivator {

	private static BundleContext context;

	static BundleContext getContext() {
		return context;
	}

	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		Hashtable<String,Object> dict = new Hashtable<String,Object>();
	    context.registerService(IDzwiek.class.getName(), new DzwiekImp(), dict);
	}

	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
	}

}

