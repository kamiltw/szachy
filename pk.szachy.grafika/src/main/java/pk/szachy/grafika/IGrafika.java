/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.szachy.grafika;

import java.awt.Color;
import java.lang.reflect.Method;
import java.net.URL;
import javax.swing.JPanel;

/**
 *
 * @author Kamil
 */
public interface IGrafika {
    public static boolean PRZENOS =true;
    public static boolean NIEPRZENOS =false;

    void ustawRozmiarPanelu(int x, int y);

    Object podajObjekt(int nr_grafiki);

    void dodajFuncjeKliknieciaNaObiekt(int nr_grafiki, Method m);

    void dodajFuncjeNajechanoNaObiekt(int nr_grafiki, Method m);

    void dodajFuncjePrzesunietoObiekt(int nr_grafiki, Method m);

    void dodajFuncjeZjechanoZObiektu(int nr_grafiki, Method m);

    int dodajGrafike(int x, int y, String url);

    int dodajGrafike(int x, int y, String url, boolean movable);
    
    int dodajGrafike(int x, int y, String url, boolean movable, Object obj);

    JPanel podajPanel();

    void przenies(int nr_grafiki, int x, int y);

    void refresh();

    void setMovable(int nr_grafiki, boolean m);

    void skaluj(int nr_grafiki, double x);

    void usunGrafike(int nr_grafiki);

    void ustawWidocznosc(int nr_grafiki, boolean widocznosc);

    public int podajGrafikeNaPozycji(int x, int y);
           
    public void ustawRozmiarGrafiki(int nr_grafiki, int x, int y);
    
    public void ustawTlo(Color c);
    
    public void przesunNaWierzch(int nr_grafiki);
    
    public void skalowaniePodczasPrzenoszenia(double scale);
    
    public int podajLiczbeGrafik();


    void zmienObrazGrafiki(int nr_grafiki, String url);
}
