/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.szachy.grafika.komponentgraficzny;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

/**
 *
 * @author Kamil
 */
public class Obrazek extends JPanel implements MouseListener, MouseMotionListener {

    private static int numberOfGrafika = 0;
    private static double przeskalujPodczasPrzenoszenia = 1.0;
    public static JPanel parent;
    public static KomponentGraficzny grafika;
    private final int id;
    private final Point poz;
    public final Object obj;
    private final Dimension rozmiar;
    private Dimension rozmiar_przed_skalowaniem;
    private ImageIcon icon;
    private boolean visible;
    public boolean movable;

    private int mouseOffsetY;
    private int mouseOffsetX;
    private Method myszKliknieto;
    private Method myszPrzesunieto;
    private Method myszNajechano;
    private Method myszWyjechano;

    public Obrazek(int x, int y, String url, boolean movable, Object obj) {
        this.obj = obj;
        this.movable = movable;
        id = numberOfGrafika++;
        rozmiar = new Dimension();
        poz = new Point(x, y);
        try {
            icon = new ImageIcon(url);
        } catch (Exception e) {
            Logger.getAnonymousLogger().log(Level.WARNING, "Blad ladowania pliku: " + url);
        }
        rozmiar.width = icon.getIconWidth();
        rozmiar.height = icon.getIconHeight();
        setBounds(x, y, rozmiar.width, rozmiar.height);
        setSize(rozmiar.width, rozmiar.height);
        setLocation(x, y);
        visible = true;

        addMouseListener(this);
    }

    static void skalujPodczasPrzenoszenia(double scale) {
        przeskalujPodczasPrzenoszenia = scale;
    }

    public int getId() {
        return id;
    }

    @Override
    public void paintComponent(Graphics g) {
        if (visible == false) {
            return;
        }
        Graphics2D g2 = (Graphics2D) g;
        g2.drawImage(icon.getImage(), 0, 0, rozmiar.width, rozmiar.height, null);

    }

    public void zmien_rozmiar(int wys, int szer) {
        rozmiar.height = wys;
        rozmiar.width = szer;
        setSize(rozmiar.width, rozmiar.height);
        parent.repaint();
    }

    public void zmien_pozycje(int x, int y) {
        setLocation(x, y);
        poz.x = x;
        poz.y = y;
    }

    public void skaluj(double i) {
        zmien_rozmiar((int) (rozmiar.width * i), (int) (rozmiar.height * i));
    }

    void widocznosc(boolean w) {
        setVisible(w);
        visible = w;
    }

    public void setMovable(boolean movable) {
        this.movable = movable;
    }

    public Object getObject() {
        return obj;
    }

    public void setImage(String url) {
        icon = new ImageIcon(url);
        rozmiar.width = icon.getIconWidth();
        rozmiar.height = icon.getIconHeight();
        setBounds(poz.x, poz.y, rozmiar.width, rozmiar.height);
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        if (movable == false) {
            return;
        }

        Object[] args = {};
        if (myszKliknieto != null) {
            try {
                myszKliknieto.invoke(obj, args);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                Logger.getAnonymousLogger().log(Level.WARNING, "Biblioteka powinna otrzymac objekt na ktorym ma byc wywolana metoda myszClicked");

            }
        }
    }

    @Override
    public void mousePressed(MouseEvent me) {
        if (movable == false) {
            return;
        }
        //przesun na wierzch
        parent.setComponentZOrder(this, 0);

        //skalowanie 
        rozmiar_przed_skalowaniem = new Dimension(rozmiar);
        skaluj(przeskalujPodczasPrzenoszenia);

        this.setCursor(Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));

        //wlaczanie funkcji obslugi zdarzen myszy
        this.addMouseMotionListener(this);

        //obliczanie pozycji wzgledem ekranu
        mouseOffsetX = me.getXOnScreen() - getLocation().x;
        mouseOffsetY = me.getYOnScreen() - getLocation().y;

    }

    @Override
    public void mouseReleased(MouseEvent me) {
        if (movable == false) {
            return;
        }
        //wraca do normalnych rozmiarow jesli byl skalowany podczas klikniecia
        zmien_rozmiar(rozmiar_przed_skalowaniem.height, rozmiar_przed_skalowaniem.width);

        //wylacza funkcje zdarzen myszy
        this.removeMouseMotionListener(this);

        try {
            this.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
            if (myszPrzesunieto == null) {
                setLocation(poz);
                return;
            }

            Object[] args = {poz.x, poz.y, me.getXOnScreen() - mouseOffsetX, me.getYOnScreen() - mouseOffsetY};
            myszPrzesunieto.invoke(obj, args);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getAnonymousLogger().log(Level.WARNING, "Biblioteka powinna otrzymac objekt na ktorym ma byc wywolana metoda myszPrzesunieto");

        }

    }

    //najechano na obrazek
    @Override
    public void mouseEntered(MouseEvent me) {

        if (myszNajechano == null) {
            return;
        }
        if (obj == null) {
            Logger.getAnonymousLogger().log(Level.WARNING, "Biblioteka powinna otrzymac objekt na ktorym ma byc wywolana metoda myszNajechano");
            return;
        }

        try {
            Object[] args = {};
            myszNajechano.invoke(obj, args);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | SecurityException ex) {
            Logger.getAnonymousLogger().log(Level.WARNING, "Blad wywolania metody myszNajechano");

        }
    }

    //zjechano z obrazka
    @Override
    public void mouseExited(MouseEvent me) {

        if (myszWyjechano == null) {
            return;
        }
        try {
            Object[] args = {};
            myszWyjechano.invoke(obj, args);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getAnonymousLogger().log(Level.WARNING, "Biblioteka powinna otrzymac objekt na ktorym ma byc wywolana metoda myszWyjechano");

        }
    }

    //podczas przesuwania
    @Override
    public void mouseDragged(MouseEvent me) {
        setLocation(me.getXOnScreen() - mouseOffsetX, me.getYOnScreen() - mouseOffsetY);
        parent.repaint();
    }

    @Override
    public void mouseMoved(MouseEvent me) {
    }

    public void setMyszKliknieto(Method myszKliknieto) {
        this.myszKliknieto = myszKliknieto;
    }

    public void setMyszPrzesunieto(Method myszPrzesunieto) {
        this.myszPrzesunieto = myszPrzesunieto;
    }

    public void setMyszNajechano(Method myszNajechano) {
        this.myszNajechano = myszNajechano;
    }

    public void setMyszWyjechano(Method myszWyjechano) {
        this.myszWyjechano = myszWyjechano;
    }

}
