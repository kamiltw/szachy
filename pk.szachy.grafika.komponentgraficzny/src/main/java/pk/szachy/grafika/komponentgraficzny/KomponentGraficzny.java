/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.szachy.grafika.komponentgraficzny;

import pk.szachy.grafika.IGrafika;
import java.awt.Color;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.HashMap;
import javax.swing.JPanel;

/**
 *
 * @author Kamil
 */
public class KomponentGraficzny implements IGrafika {

    private final HashMap<Integer, Obrazek> grafiki;
    private final JPanel panel;
    private static int numberOfGraphics = 0;

    public KomponentGraficzny() {
        grafiki = new HashMap<>();

        panel = new JPanel();
        Obrazek.parent = panel;
        //      Obrazek.grafika=this;
        panel.setLayout(null);
        panel.setBackground(Color.yellow);
        panel.setSize(100, 100);
        panel.setVisible(true);
    }

    @Override
    public void ustawRozmiarPanelu(int x, int y) {
        panel.setSize(x, y);
    }

    @Override
    public int dodajGrafike(int x, int y, String url) {
        return dodajGrafike(x, y, url, false, null);
    }

    @Override
    public int dodajGrafike(int x, int y, String url, boolean movable) {
        return dodajGrafike(x, y, url, movable, null);
    }

    @Override
    public int dodajGrafike(int x, int y, String url, boolean movable, Object obj) {
        Obrazek tmp = new Obrazek(x, y, url, movable, obj);
        grafiki.put(numberOfGraphics, tmp);
        panel.add(tmp, 0);
        refresh();
        return numberOfGraphics++;
    }

    @Override
    public void skaluj(int nr_grafiki, double x) {
        Obrazek obr = grafiki.get(nr_grafiki);
        if (obr == null) {
            return;
        }
        obr.skaluj(x);
        refresh();
    }

    @Override
    public void przenies(int nr_grafiki, int x, int y) {
        Obrazek obr = grafiki.get(nr_grafiki);
        if (obr == null) {
            return;
        }
        obr.zmien_pozycje(x, y);
        refresh();
    }

    @Override
    public void usunGrafike(int nr_grafiki) {
        Obrazek obr = grafiki.get(nr_grafiki);
        if (obr == null) {
            return;
        }
        panel.remove(grafiki.remove(nr_grafiki));
        refresh();
    }

    @Override
    public void ustawWidocznosc(int nr_grafiki, boolean widocznosc) {
        Obrazek obr = grafiki.get(nr_grafiki);
        if (obr == null) {
            return;
        }
        obr.widocznosc(widocznosc);
        panel.repaint();
    }

    @Override
    public Object podajObjekt(int nr_grafiki) {
        Obrazek obr = grafiki.get(nr_grafiki);
        if (obr == null) {
            return null;
        }
        return obr.getObject();
    }

    @Override
    public JPanel podajPanel() {
        return panel;
    }

    @Override
    public void dodajFuncjeKliknieciaNaObiekt(int nr_grafiki, Method m) {
        Obrazek obr = grafiki.get(nr_grafiki);
        if (obr == null) {
            return;
        }
        obr.setMyszKliknieto(m);
    }

    @Override
    public void dodajFuncjePrzesunietoObiekt(int nr_grafiki, Method m) {
        Obrazek obr = grafiki.get(nr_grafiki);
        if (obr == null) {
            return;
        }
        obr.setMyszPrzesunieto(m);
    }

    @Override
    public void dodajFuncjeNajechanoNaObiekt(int nr_grafiki, Method m) {
        Obrazek obr = grafiki.get(nr_grafiki);
        if (obr == null) {
            return;
        }
        obr.setMyszNajechano(m);
    }

    @Override
    public void dodajFuncjeZjechanoZObiektu(int nr_grafiki, Method m) {
        Obrazek obr = grafiki.get(nr_grafiki);
        if (obr == null) {
            return;
        }
        obr.setMyszWyjechano(m);
    }

    @Override
    public void setMovable(int nr_grafiki, boolean m) {
        Obrazek obr = grafiki.get(nr_grafiki);
        if (obr == null) {
            return;
        }
        obr.setMovable(m);
    }

    @Override
    public void refresh() {
        panel.repaint();
    }

    @Override
    public int podajGrafikeNaPozycji(int x, int y) {
        Object ob = panel.findComponentAt(x, y);
        if (ob == null || ob.equals(panel)) {
            return -1;
        } else {
            return ((Obrazek) ob).getId();
        }
    }

    @Override
    public void ustawRozmiarGrafiki(int nr_grafiki, int x, int y) {
        Obrazek obr = grafiki.get(nr_grafiki);
        if (obr == null) {
            return;
        }
        obr.zmien_rozmiar(x, y);
    }

    @Override
    public void ustawTlo(Color c) {
        panel.setBackground(c);
    }

    @Override
    public void przesunNaWierzch(int nr_grafiki) {
        panel.setComponentZOrder((JPanel) grafiki.get(nr_grafiki), 0);
    }

    @Override
    public void skalowaniePodczasPrzenoszenia(double scale) {
        Obrazek.skalujPodczasPrzenoszenia(scale);
    }

    @Override
    public int podajLiczbeGrafik() {
        return numberOfGraphics;
    }

    @Override
    public void zmienObrazGrafiki(int nr_grafiki, String url) {
         Obrazek obr = grafiki.get(nr_grafiki);
        if (obr == null) {
            return;
        }
        obr.setImage(url);
    }

}
