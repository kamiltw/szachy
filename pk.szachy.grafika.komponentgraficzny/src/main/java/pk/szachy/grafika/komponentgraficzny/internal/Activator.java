package pk.szachy.grafika.komponentgraficzny.internal;

import java.util.Hashtable;

import javax.swing.ImageIcon;

import pk.szachy.grafika.IGrafika;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import javax.swing.JOptionPane;
import pk.szachy.grafika.komponentgraficzny.KomponentGraficzny;

/**
 * Serwis Grafiki
 **/

public class Activator implements BundleActivator {

	private static BundleContext context;

	static BundleContext getContext() {
		return context;
	}

	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		Hashtable<String,Object> dict = new Hashtable<String,Object>();
	    context.registerService(IGrafika.class.getName(), new KomponentGraficzny(), dict);
	}

	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
	}

}

